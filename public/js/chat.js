var currentGroup;
var user_email;
var user_uid;

function init() {
    user_email = '';
    user_uid = '';
    currentGroup = "Public";

    var groupName = document.getElementById("groupName")
    groupName.innerHTML = currentGroup;

    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            user_uid = user.uid;
    
            checkGroup(user);
            updateGroupBtn(user);
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
          
            var btnLogout = document.getElementById('logout-btn');

            btnLogout.addEventListener('click', function() {
                    alert("success","logout success");
                    location.replace('index.html');
                    localStorage.clear();
                    firebase.auth().signOut();
                    user = null;
                    console.log("success");
                   
               
            }).catch(function(error) {
                // Handle Errors here.
                create_alert("error","logout failed");
                console.log("fail");
            });


        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='index.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    create_btn = document.getElementById('btnCreate');
    create_btn.addEventListener('click', function(){

        var group = prompt("Please enter group name:", "New Group");

        if (group == null || group == "") {
            alert("Group create failed");
        } 

        else {
            
            var groupRef = firebase.database().ref('group/' + group + '/member/' + user_uid );
            var userRef = firebase.database().ref('user/' + user_uid + '/usersGroup/' + group);
            
            groupRef.push({
                email: user_email,
                role : 'admin'
            });

            userRef.push({
                role: 'admin'
            })

            database.ref('group/' + group + '/com_list').push({
                email: "system",
                data : "This group is created by " + user_email
            });

            alert("Group create success");
        }

    });

    img_btn = document.getElementById('img_btn');

    img_btn.addEventListener('change', function(e){
        /// TODO 2: Put the image to storage, and push the image to database's "com_list" node
        ///         1. Get the reference of firebase storage
        ///         2. Upload the image
        ///         3. Get the image file url, the reference of "com_list" and push user email and the image
   
        var storage = firebase.storage();

        var storageRef = storage.ref();

        
        var file = this.files[0];

        
    
        var imagesRef = storageRef.child((Math.random() * 10000000000)+file.name);
        
            
            imagesRef.put(file).then(function(snapshot) {
                var URL = imagesRef.getDownloadURL().then(function(url1){
                    console.log("donw");
                    var Ref = firebase.database().ref('com_list');
                    console.log("test");
                    var data = {
                        email: user_email,
                        // Type 0 for comment
                        type: 1,
                        data: '',
                        url: url1
                    };
                    firebase.database().ref("group/" + currentGroup + "/com_list").push(data);
                    console.log("url: " +url1);

                });
            });
        
        
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    comment = post_txt.value;
    var database = firebase.database();


    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {

            
         
        
            database.ref('group/' + currentGroup + '/com_list').push({
                type: 0,
                url:'',
                email: user_email,
                data : post_txt.value
            });
            //console.log(post_txt.value);
            post_txt.value = "";
        

           
        }
    });

    // The html code for post
    var str_before_username = "<div class='media text-muted pt-3'><img src='image/icon.jpg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><div class='my-2 p-2  bg-white rounded box-shadow'><p class='media-body pb-2 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</div></div>\n";
    var str_before_img = "</p><p class='media-body pb-2 mb-0 small lh-125 message'></p><img class='img pt-2' style='height: 300px;' src='";
    var str_after_img = "'>";
    var postsRef = firebase.database().ref('group/' + currentGroup + '/com_list');
    // List for store posts html
    var total_post = [];
    var message =[];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded 
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. count history message number and recond in "first_count"
            ///         Hint : Trace the code in this block, then you will know how to finish this TODO

            /// Join all post in list to html in once
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                var n = total_post.length;
                if(childData.type != 1)
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + "</p><p class='media-body pb-2 mb-0 small lh-125 message'></p>" + str_after_content;

                else{
                    console.log("pict");
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url+str_after_img  + str_after_content;
                }
                message[n] = childData.data
                first_count += 1;
                console.log("n=" + n);
            });


            document.getElementById('post_list').innerHTML = total_post.join('');

            console.log(total_post.length);

            var chat = document.getElementsByClassName("message");
            var i = 0;

            for(var i = 0; i<total_post.length;i++){
                chat[i].innerText = message[i];
                console.log("i" + message[i]);
                
            }


            /// Add listener to update new post

            postsRef.on('child_added', function(data) {
                second_count += 1;
                //console.log(first_count+" "+ second_count);
                if (second_count > first_count) {
                  
                    var childData = data.val();
                    
                    if(childData.type != 1)
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + "</p><p class='media-body pb-2 mb-0 small lh-125 message'></p>" + str_after_content;

                    else{
                        console.log("pict2");
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url+str_after_img + str_after_content;
                    }
                    message[total_post.length-1] = childData.data;
                    document.getElementById('post_list').innerHTML = total_post.join('');

                    var i = 0;

                for(var i = 0; i<total_post.length;i++){
                    chat[i].innerText = message[i];
                    console.log("i" + message[i]);
                    
                }
                    
                    //var title = "JavaScript Jeep";
                    if (Notification.permission === "granted" && document.visibilityState !== "visible") {
                        // If it's okay let's create a notification
                      
                        var notification = new Notification(childData.email + " ("+ currentGroup+"):\n\n" + childData.data);
                      }
                    
                      // Otherwise, we need to ask the user for permission
                      else if (Notification.permission !== "denied" && document.visibilityState !== "visible") {
                        Notification.requestPermission().then(function (permission) {
                          // If the user accepts, let's create a notification
                          if (permission === "granted") {
                            var notification = new Notification(childData.email + ":\n\n" + childData.data);
                          }
                        });
                    }
                
                }
            });


        })
        .catch(e => console.log(e.message));

   


}

window.onload = function() {
    init();
};

function checkGroup(user){
    
    var publicRef = firebase.database().ref('group/Public/member/' + user.uid);
    var currentRef = firebase.database().ref('user/' + user_uid + '/usersGroup/Public');

    publicRef.once("value").then(function(snapshot) {

        if(snapshot.val()==null){
            
            console.log(snapshot.child(user.uid).val());
            publicRef.push({
                email: user.email,
                role : 'member'
            });

            currentRef.push({
                role: 'member'
            })
        }
  });
}

function updateGroupBtn(user){
    var str_before = "<div><button class='btn btn-lg btn-secondary btn-block' id = 'groupBtn' value ='";
    var str_event = " onclick= \"changeGroup(";
    var str_after = "</button></div>";

    var postsRef = firebase.database().ref('group');

    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    var user = firebase.auth().currentUser;
    var user_uid2 = user.uid;
 
    // if(user!=null){
    //     user_uid2 = user.uid;
    //     console.log("uid: " + user_uid2);
    // }
    

    var userRef = firebase.database().ref('user/' + user_uid2 + '/usersGroup');
    console.log("uid: " + user_uid2);

    userRef.once('value').then(function(snap){

        snap.forEach(function(cs){
            console.log("hi " + cs.key);
                
            console.log("yes");
            total_post[total_post.length] = str_before + cs.key +"'"+ str_event +"'"+ cs.key + "')\">"+ cs.key + str_after;
            console.log("QQ "+cs.key+" " + total_post.length);
            console.log(total_post);
            first_count += 1;
            
        })

        document.getElementById('group').innerHTML = total_post.join('');
        console.log("len1: " + total_post.length);
        

        userRef.on('child_added', function(data) {
            second_count += 1;
            var key = data.key;
            console.log(first_count +" " + second_count);
            if (second_count > first_count) {
                // console.log("len: " + total_post.length);
                
                total_post[total_post.length] = str_before + key +"'"+ str_event +"'"+ key + "')\">"+ key + str_after;
                console.log("child:" + key);
                document.getElementById('group').innerHTML = total_post.join('');
                
            
            }
        });

        userRef.on('child_removed', function(data) {
            updateGroupBtn();
        });
        
    });
        
        
}

function changeGroup(value){
    currentGroup = value;
    var groupName = document.getElementById("groupName")
    groupName.innerHTML = currentGroup;


    var str_before_username = "<div class='media text-muted pt-3'><img src='image/icon.jpg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><div class='my-2 p-2  bg-white rounded box-shadow'><p class='media-body pb-2 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</div></div>\n";
    var str_before_img = "</p><p class='media-body pb-2 mb-0 small lh-125 message'></p><img class='img pt-2' style='height: 300px;' src='";
    var str_after_img = "'>";
    var postsRef = firebase.database().ref('group/' + currentGroup + '/com_list');
    // List for store posts html
    var total_post = [];
    var message =[];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded 
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. count history message number and recond in "first_count"
            ///         Hint : Trace the code in this block, then you will know how to finish this TODO

            /// Join all post in list to html in once
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                var n = total_post.length;
                if(childData.type != 1)
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + "</p><p class='media-body pb-2 mb-0 small lh-125 message'></p>" + str_after_content;

                else{
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url+str_after_img  + str_after_content;
                }
                message[n] = childData.data
                first_count += 1;
                //console.log("n=" + n);
            });


            document.getElementById('post_list').innerHTML = total_post.join('');

            console.log(total_post.length);

            var chat = document.getElementsByClassName("message");
            var i = 0;

            for(var i = 0; i<total_post.length;i++){
                chat[i].innerText = message[i];
                //console.log("i" + message[i]);
                
            }


            /// Add listener to update new post

            postsRef.on('child_added', function(data) {
                second_count += 1;
                //console.log(first_count+" "+ second_count);
                if (second_count > first_count) {
                  
                    var childData = data.val();
                    
                    if(childData.type != 1)
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + "</p><p class='media-body pb-2 mb-0 small lh-125 message'></p>" + str_after_content;

                    else{
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url+str_after_img + str_after_content;
                    }
                    message[total_post.length-1] = childData.data;
                    document.getElementById('post_list').innerHTML = total_post.join('');

                    var i = 0;

                for(var i = 0; i<total_post.length;i++){
                    chat[i].innerText = message[i];
                    console.log("i" + message[i]);
                    
                }
                    
                    //var title = "JavaScript Jeep";
                    if (Notification.permission === "granted" && document.visibilityState !== "visible") {
                        // If it's okay let's create a notification
                      
                        var notification = new Notification(childData.email + " ("+ currentGroup+"):\n\n" + childData.data);
                      }
                    
                      // Otherwise, we need to ask the user for permission
                      else if (Notification.permission !== "denied" && document.visibilityState !== "visible") {
                        Notification.requestPermission().then(function (permission) {
                          // If the user accepts, let's create a notification
                          if (permission === "granted") {
                            var notification = new Notification(childData.email + ":\n\n" + childData.data);
                          }
                        });
                    }
                
                }
            });


        })
        .catch(e => console.log(e.message));

}

function addMember(){

    if(currentGroup == "Public"){
        alert("Can't add group to public");
        return;
    }
    var newMember = prompt("Please enter a member's email:", "");
    var isMember = false;
    var newMember_uid;
    // var newUid = firebase.auth().fetchProvidersForEmail(newMember);
    // console.log(newUid);
    
    // console.log(newMember);
    var postsRef = firebase.database().ref('group/Public/member');

    

    if (newMember == null || newMember == "") {
        alert("Failed to add new member");
        return;
    } 

    postsRef.once('value')
    .then(function(snapshot) {

        snapshot.forEach(function(childshot) {
            var childKey = childshot.key;

            var childRef = firebase.database().ref('group/Public/member/' + childKey);
            console.log(childKey);

            childRef.once('value')
            .then(function(snapshot2){
        
                snapshot2.forEach(function(childshot) {
                    var childData = childshot.val();
                    var email = childData.email;
        
                   if(email === newMember){

                    newMember_uid = childKey;    
                    console.log("in = " + newMember);
                    console.log(email);
                        
                        var memberRef = firebase.database().ref('group/' + currentGroup + '/member/' + newMember_uid);

                        console.log(childData.email === newMember);

                        memberRef.once("value").then(function(snapshot3) {
                            // console.log(snapshot3);

                            if(snapshot3.val()==null){

                                
                                
                                alert(newMember + " has been added to the group");

                                firebase.database().ref('group/' + currentGroup + '/com_list').push({
                                    email: "system",
                                    data : newMember + " has been added to the group."
                                });
                                
                                memberRef.push({
                                    email: newMember,
                                    role : 'member'
                                });

                                firebase.database().ref('user/' + newMember_uid +'/usersGroup/' + currentGroup).push({
                                    role: 'member'
                                });

                                console.log("member "+newMember_uid);
                            }

                            // else{
                            //     alert(newMember + " is already in the group");
                            //     isMember = false;
                            // }
                        });
                   }

                   
                });

                // if(isMember ==false){
                //     alert("entered email is not a member");
                // }
            });
            
        });

        
    });
}

function kickMember(){
    var groupRef = firebase.database().ref('group/' + currentGroup + '/member/' + user_uid);
    var postsRef = firebase.database().ref('group/' + currentGroup + '/member');

    if(currentGroup == "Public"){
        alert("Unable to kick members in Public chat");
        return;
    }

    var isMember = false;
    console.log("hello:" + user_uid);

    groupRef.once('value')
    .then(function(snapshot){

        snapshot.forEach(function(childshot) {
            var data = childshot.val();
            console.log(data.role);

            if(data.role == "admin"){

                var member = prompt("Please enter a member's email:", "");

                postsRef.once('value')
                .then(function(snapshot) {

                    snapshot.forEach(function(childshot) {
                        var childKey = childshot.key;

                        var childRef = firebase.database().ref('group/' + currentGroup + '/member/' + childKey);
                        console.log(childKey);

                        childRef.once('value')
                        .then(function(snapshot2){
                    
                            snapshot2.forEach(function(childshot) {
                                var childData = childshot.val();
                                console.log("in = " + member);
                                console.log(childData.email);
                    
                                if(childData.email === member){
                                    Member_uid = childKey;

                                    firebase.database().ref('group/' + currentGroup + '/com_list').push({
                                        email: "system",
                                        data : member + " has been removed from the group."
                                    });

                                    firebase.database().ref('group/' + currentGroup + '/member/' + Member_uid).remove();
                                    firebase.database().ref('user/' + Member_uid +'/usersGroup/' + currentGroup).remove();

                                    

                                    alert(member + " has been removed from the group")
                                
                                        
                                }

                            

                            });
                        });
                        
                    });

                    
                });
            }

            else{
                alert("only the admin can kick a member");
            }

            
        });

        
    });
    
}

function leaveGroup(){
    var groupRef = firebase.database().ref('group/' + currentGroup + '/member/' + user_uid);
    var checkRef = firebase.database().ref('group/' + currentGroup + '/member');

    if(currentGroup == "Public"){
        alert("Unable to leave Public chat");
        return;
    }


    groupRef.once('value')
    .then(function(snapshot){

        snapshot.forEach(function(childshot) {
            var data = childshot.val();
            console.log(data.role);

            if(data.role == "admin"){

                var sure = confirm("You are the admin of the group. If you leave the group the whole group will be deleted. Are you sure you want to leave?");
                console.log("sure: "+ sure);
                if(sure){
                    
                    var prevGroup = currentGroup;

                    firebase.database().ref('group/' + prevGroup + '/com_list').push({
                        email: "system",
                        data : "This group is deleted and is no longer available."
                    });

                    checkRef.once('value').then(function(snapshot1){
                        snapshot1.forEach(function (childData){
                            key = childData.key;
                            console.log("this :" + key);

                            firebase.database().ref('user/' + key +'/usersGroup/' + prevGroup).remove();
                        });
                        firebase.database().ref('group/' + prevGroup).remove();
                    });
                    
                    
                    changeGroup("Public");
                    // location.reload();
    
                }
            }

            else{
                var sure = confirm("Are you sure you want to leave the group?");

                if(sure){
                    firebase.database().ref('group/' + currentGroup + '/com_list').push({
                        email: "system",
                        data : user_email + " has left the group."
                    });

                    console.log(user_email);

                    firebase.database().ref('group/' + currentGroup +'/member/' + user_uid).remove();
                    firebase.database().ref('user/' + user_uid +'/usersGroup/' + currentGroup).remove();
                    changeGroup("Public");
          
                }
            }


        });

        
    });
}
