# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [Chatroom]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://chatroom-ccad0.web.app

## Website Detail Description
In the login, users can log in through google or email(after sign up). If user is not a member, user can sign up through the sign up button.
After login, user can always logout through the button "Account".

<img src="image/Logout.png">

The current group name is located at the top of the chatroom. "Public" will be the default group and you will be added to the group once you successfully login to you account. You can send messages in the group and history chat will be loaded. Users can also send images through the "Upload Image" button.

Users can also create a private group with the button "Create Group" located on the left side of the screen. After succesfully creating a group, the user will be set as the admin of the group. Once entered a group, members can leave anytime and only the admin can kick a member.

<img src="image/createGroup.png">


# Components Description : 
1. Membership Mechanism : 

    Users who is not a member can always sign up by entering the email and password and press the "New Account" button. If user is already a member, user can sign in using the "Sign Up" button.

2. Firebase Page : 

    firebase deploy with url: https://chatroom-ccad0.web.app

3. Database :

    Informations of usres and groups are stored and read from database.

    <img src="image/database.png">

4. RWD :

    This website is able to retain it's tidiness if there is changes to the width os the website.

5. Topic Key Function : 

    Members of a group can chat by sending text messages through the textbox below. Users can also create a private group with the button "Create Group" located on the left side of the screen. After succesfully creating a group, the user will be set as the admin of the group. Here only members added can view and send messages in the private group. All members can add new members to the group. Everytime the web page is loaded or whenever the user changes group, the chat history of the current group will be loaded.

    <img src="image/addMember.png">

6. Third-Party Sign In :

    This website also support Google Sign In method.

7. Chrome Notification :

    To prevent notification from popping too many times. Notification will only activate if the web page is not visible (eg. user switched to other tabs). Notification is available only for the group the users is currently looking at (eg. User is currently looking at group "Public", messages from other groups will not trigger notifications). If there are new message in the group, notification be activated.

8. Use CSS Animation :

    There are animations when users sign in into the chat room. Besides, when the width of the website is small, the top right button will have some animations when pressed.

9. Security Report : 

    html tags will be converted to text messages to prevent it from altering the webpage.

    <img src="image/htmlError.png">

# Other Functions Description : 
1. Uploading image in groups :

    Users can upload image in groups by using the "Upload Image" button below.

2. Leave Group Function : 

    Members can leave a private group anytime (can't leave "Public" group, there will be an alert) by clicking the "Leave Group" button above. When pressed, there will be a confirm pop up to make sure the user confirms to leave. Once leave, members will not be able to return unless he or she is added back to the group.

    <img src="image/leave.png">

    If the user is a admin, the group will be deleted once confirm, every members in the group will not be able to access the group again.

3. Kick Member function : 

    Admin has a special authority in a private group. Only admin can choose to kick a member. If the user is not the admin, kicking member is not allowed and it will trigger an alert.

    <img src="image/kick.png">

